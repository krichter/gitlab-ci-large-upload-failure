#!/usr/bin/python

import random
import sys

def main(mb_count):
    for i in range(0, 1024*1024*mb_count):
        sys.stdout.write(str(int(random.random()*10)))

if __name__ == "__main__":
    main(60)
